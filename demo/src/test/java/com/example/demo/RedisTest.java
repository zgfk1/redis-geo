package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import redis.clients.jedis.*;
import redis.clients.jedis.params.GeoRadiusParam;

import java.util.*;

@Slf4j
public class RedisTest {
    Jedis localRedis;
    private static JedisPool jedisPool = null;
    // 访问密码
    private static final String AUTH = null;
    private static final String KEY = "company";


    static {
        try {
            JedisPoolConfig config = new JedisPoolConfig();
            // 连接耗尽时是否阻塞, false报异常,ture阻塞直到超时, 默认trueconfig.setBlockWhenExhausted(true);
            // 设置的逐出策略类名, 默认DefaultEvictionPolicy(当连接超过最大空闲时间,或连接数超过最大空闲连接数)
            config.setEvictionPolicyClassName("org.apache.commons.pool2.impl.DefaultEvictionPolicy");
            // 是否启用pool的jmx管理功能, 默认trueconfig.setJmxEnabled(true);
            // 最大空闲连接数, 默认8个 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。config.setMaxIdle(8);
            // 最大连接数, 默认8个
            config.setMaxTotal(200);
            // 表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；config.setMaxWaitMillis(1000 * 100);
            // 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
            config.setTestOnBorrow(true);
            // Redis服务器IP
            String addr = "127.0.0.1";
            // Redis的端口号
            int port = 6379;
            jedisPool = new JedisPool(config, addr, port, 3000, AUTH);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 首先执行这个方法
     * 通过geoadd初始化坐标信息
     */
    @Test
    public void addGEO() {
        // 直接初始化
        localRedis.geoadd(KEY, 116.48105, 39.996794, "juejin");
        localRedis.geoadd(KEY, 116.514203, 39.905409, "ireader");
        localRedis.geoadd(KEY, 116.489033, 40.007669, "meituan");


        Jedis resource = jedisPool.getResource();
        // 通过map初始化
        Map<String, GeoCoordinate> data = new HashMap<>(2);
        data.put("jd", new GeoCoordinate(116.562108, 39.787602));
        data.put("xiaomi", new GeoCoordinate(116.334255, 40.027400));
        resource.geoadd(KEY, data);
    }

    /**
     * 通过 geodist 计算距离
     */
    @Test
    public void selectDistance() {
        Double geodist = localRedis.geodist(KEY, "jd", "xiaomi", GeoUnit.KM);
        log.info("京东和小米之间距离：{}公里", geodist);
        // 京东和小米之间距离：33.0047公里
        Double m = localRedis.geodist(KEY, "jd", "xiaomi", GeoUnit.M);
        log.info("京东和小米之间距离：{}米", m);

        Double mi = localRedis.geodist(KEY, "jd", "xiaomi", GeoUnit.MI);
        log.info("京东和小米之间距离：{}米", mi);

    }

    /**
     * 通过 geopos 获取坐标信息
     */
    @Test
    public void getData() {
        // 获取元素的坐标
        List<String> companys = Arrays.asList("jd", "xiaomi");
        for (String company : companys) {
            List<GeoCoordinate> geopos = localRedis.geopos(KEY, company);
            log.info("{} {}", company, geopos.get(0));
        }
    }

    /**
     * 通过georadiusByMember计算附近的坐标点（附近的公司）
     */
    @Test
    public void radiusParam() {
        // 查询附近的公司
        GeoRadiusParam geoRadiusParam = GeoRadiusParam.geoRadiusParam().count(3).sortAscending();
        List<GeoRadiusResponse> ireaderNears = localRedis.georadiusByMember(KEY, "ireader", 20, GeoUnit.KM, geoRadiusParam);
        log.info("掌阅附近的公司由近及远: ");
        for (int i = 0; i < ireaderNears.size(); i++) {
            log.info("{} ) {}", i, ireaderNears.get(i).getMemberByString());
        }
        log.info("掌阅附近的公司由远及近: ");
        geoRadiusParam = GeoRadiusParam.geoRadiusParam().count(3).sortDescending();
        ireaderNears = localRedis.georadiusByMember(KEY, "ireader", 20, GeoUnit.KM, geoRadiusParam);
        for (int i = 0; i < ireaderNears.size(); i++) {
            log.info("{} ) {}", i, ireaderNears.get(i).getMemberByString());
        }
        // withdist 显示距离，withCoord 显示坐标
        geoRadiusParam = GeoRadiusParam.geoRadiusParam().count(3).withCoord().withDist();
        ireaderNears = localRedis.georadiusByMember(KEY, "ireader", 20, GeoUnit.KM, geoRadiusParam);
        log.info("掌阅附近的公司: ");
        for (int i = 0; i < ireaderNears.size(); i++) {
            log.info("{} ) {}", i, ireaderNears.get(i).getMemberByString());
            log.info("坐标：{}", ireaderNears.get(i).getCoordinate());
            log.info("距离：{}", ireaderNears.get(i).getDistance());
        }
    }

    /**
     * 半径范围内存在的数据
     */
    @Test
    public void georadiusByMember() {

        List<GeoRadiusResponse> geoRadiusResponses = localRedis.georadiusByMember(KEY, "ireader", 40, GeoUnit.KM);
        log.info("掌阅40公里内的公司: ");
        for (int i = 0; i < geoRadiusResponses.size(); i++) {
            log.info("{} ) {}", i, geoRadiusResponses.get(i).getMemberByString());
        }
    }

    @BeforeTest
    public void initJedis() {
        try {
            if (jedisPool != null) {
                localRedis = jedisPool.getResource();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
