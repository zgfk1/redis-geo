package com.example.demo;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.io.file.FileSystemUtil;
import cn.hutool.core.io.file.PathUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.system.SystemUtil;
import org.junit.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class FileReadTest {

	@Test
	public void readFile() {

		List<File> files = FileUtil.loopFiles("F:\\cs");

//        files.forEach(f->System.out.println(f.getAbsoluteFile()));
//        files.forEach(f->System.out.println(FileNameUtil.mainName(f.getName())));

//        files.stream().filter(f -> f.getName().endsWith(".txt") || f.getName().endsWith(".excel") || f.getName().endsWith(".exe")).forEach(f -> System.out.println(FileNameUtil.mainName(f.getName())));
		List<String> strings = new ArrayList<>();
		strings.add(".xls");
		strings.add(".txt");
		strings.add(".exe");
		//chatgpt 优化后的
		files.stream()
				.filter(f -> f.getName().matches(".*\\.(xls|excel|exe)$")).forEach(f -> {
//                    String absolutePath = f.getAbsolutePath();
//                    System.out.println(absolutePath);
//
//                    String osName = SystemUtil.get(SystemUtil.OS_NAME);
//                    System.out.println(osName);
//
					String FILE_SEPARATOR = SystemUtil.get(SystemUtil.FILE_SEPARATOR);
//                    System.out.println(FILE_SEPARATOR);

					String[] split = f.getAbsolutePath().split(FILE_SEPARATOR + FILE_SEPARATOR);
					int i = f.getAbsolutePath().lastIndexOf(FILE_SEPARATOR);
					System.out.println(split[split.length - 1]);
					String substring = f.getAbsolutePath().substring(0, f.getAbsolutePath().lastIndexOf(FILE_SEPARATOR));
					System.out.println(substring);
					String concat = substring.concat(FILE_SEPARATOR).concat("test.txt");
					File file = new File(concat);
					if (FileUtil.exist(file)) {
						System.out.println(concat.concat("  存在"));
						String s = ResourceUtil.readUtf8Str(concat);
						System.out.println(s);
						System.out.println(JSONUtil.readJSON(file, StandardCharsets.UTF_8));

					}

				});
		files.stream()
				.filter(f -> f.getName().endsWith(".xls")
						|| f.getName().endsWith(".excel")
						|| f.getName().endsWith(".exe")).forEach(f -> {
//                    String absolutePath = f.getAbsolutePath();
//                    System.out.println(absolutePath);
//
//                    String osName = SystemUtil.get(SystemUtil.OS_NAME);
//                    System.out.println(osName);
//
					String FILE_SEPARATOR = SystemUtil.get(SystemUtil.FILE_SEPARATOR);
//                    System.out.println(FILE_SEPARATOR);

					String[] split = f.getAbsolutePath().split(FILE_SEPARATOR + FILE_SEPARATOR);
					int i = f.getAbsolutePath().lastIndexOf(FILE_SEPARATOR);
					System.out.println(split[split.length - 1]);
					String substring = f.getAbsolutePath().substring(0, f.getAbsolutePath().lastIndexOf(FILE_SEPARATOR));
					System.out.println(substring);
					String concat = substring.concat(FILE_SEPARATOR).concat("test.txt");
					File file = new File(concat);
					if (FileUtil.exist(file)) {
						System.out.println(concat.concat("  存在"));
						String s = ResourceUtil.readUtf8Str(concat);
						System.out.println(s);
						System.out.println(JSONUtil.readJSON(file, StandardCharsets.UTF_8));

					}

				});
	}

	@Test
	public void listTest() {
		final FileSystem fileSystem = FileSystemUtil.createZip("d:/test/test.zip", CharsetUtil.CHARSET_GBK);
		final Path root = FileSystemUtil.getRoot(fileSystem);
		PathUtil.walkFiles(root, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
				Console.log(path);
				return FileVisitResult.CONTINUE;
			}
		});
	}
}

